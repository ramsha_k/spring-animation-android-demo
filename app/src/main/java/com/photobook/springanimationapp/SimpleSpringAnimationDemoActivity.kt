package com.photobook.springanimationapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class SimpleSpringAnimationDemoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_spring_animation)
    }
}