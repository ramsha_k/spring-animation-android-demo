package com.photobook.springanimationapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_bounded_bubble_view_demo.*

class BoundedBubbleViewDemoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_bounded_bubble_view_demo)
        imageView.setupBounds(containerLayout)
    }
}