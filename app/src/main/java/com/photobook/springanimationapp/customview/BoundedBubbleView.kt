package com.photobook.springanimationapp.customview

import android.content.Context
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import androidx.appcompat.widget.AppCompatImageView
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce

class BoundedBubbleView: AppCompatImageView {

    private var originalX = 0f
    private var originalY = 0f

    private lateinit var springAnimationX : SpringAnimation
    private lateinit var springAnimationY : SpringAnimation

    private lateinit var bounds: RectF

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        setMovingViewSpringAnimation()
        setMovingViewTouchListener()
    }

    fun setupBounds(view: View) {
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                bounds = RectF().apply {
                    left = view.x
                    right = view.width.toFloat()
                    top = view.y
                    bottom = view.height.toFloat()
                }

                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
    }

    //create and set spring animations once the view position is known
    private fun setMovingViewSpringAnimation() {
        viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                originalX = x
                originalY = y

                createXAnimation()
                createYAnimation()

                viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
    }



    //handle dragging the view
    private fun setMovingViewTouchListener() {
        var deltaX = 0f
        var deltaY = 0f

        var translateX = 0f
        var translateY = 0f

        setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(view: View, event: MotionEvent?): Boolean {
                when(event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        deltaX = view.x - event.rawX
                        deltaY = view.y - event.rawY
                    }

                    MotionEvent.ACTION_MOVE -> {
                        translateX = deltaX + event.rawX
                        translateY = deltaY + event.rawY

                        view.animate()
                            .x(translateX)
                            .y(translateY)
                            .setDuration(0)
                            .start()
                    }

                    MotionEvent.ACTION_UP -> {
                        //check for X
                        when {
                            translateX > bounds.centerX() -> {
                                springAnimationX.animateToFinalPosition(originalX)
                            }

                            else -> {
                                springAnimationX.animateToFinalPosition(bounds.left)
                            }
                        }

                        //check for Y
                        when {
                            translateY < bounds.top -> {
                                springAnimationY.animateToFinalPosition(bounds.top)
                            }

                            translateY > (bounds.bottom - height) -> {
                                springAnimationY.animateToFinalPosition(bounds.bottom - height)
                            }

                            else -> {
                                springAnimationY.animateToFinalPosition(translateY)
                            }
                        }
                    }
                }

                return true
            }
        })
    }

    private fun createXAnimation() {
        springAnimationX = SpringAnimation(this, SpringAnimation.X).apply {
            spring = SpringForce().apply {
                finalPosition = originalX
                stiffness = SpringForce.STIFFNESS_MEDIUM
                dampingRatio = SpringForce.DAMPING_RATIO_MEDIUM_BOUNCY
            }
        }
    }

    private fun createYAnimation() {
        springAnimationY = SpringAnimation(this, SpringAnimation.Y).apply {
            spring = SpringForce().apply {
                finalPosition = originalY
                stiffness = SpringForce.STIFFNESS_MEDIUM
                dampingRatio = SpringForce.DAMPING_RATIO_MEDIUM_BOUNCY
            }
        }
    }
}